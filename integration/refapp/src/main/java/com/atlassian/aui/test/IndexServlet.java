package com.atlassian.aui.test;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.OutputStreamWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IndexServlet extends HttpServlet
{
    private static final Logger log = LoggerFactory.getLogger(IndexServlet.class);

    // ------------------------------------------------------------------------------------------------------- Constants
    private static final String CONTENT_TYPE = "text/html;charset=UTF-8";
    public static final String SERVER_SOY_MODULE_KEY = "auiplugin-tests:soy-test-pages";
    // ---------------------------------------------------------------------------------------------------- Dependencies
    private final WebResourceManager webResourceManager;
    private final Plugin plugin;
    private final SoyTemplateRenderer soyTemplateRenderer;
    // ---------------------------------------------------------------------------------------------------- Constructors

    public IndexServlet(WebResourceManager webResourceManager, PluginAccessor pluginAccessor, SoyTemplateRenderer soyTemplateRenderer)
    {
        this.webResourceManager = webResourceManager;
        this.plugin = pluginAccessor.getPlugin("auiplugin-tests");
        this.soyTemplateRenderer = soyTemplateRenderer;
    }

    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators

    /**
     * Serves the base directory listing for the test pages
     * @param req {@link HttpServletRequest}
     * @param resp {@link HttpServletResponse}
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        Map<String, Object> context = Maps.newHashMap();
        context.put("webResourceManager", webResourceManager);
        resp.setContentType(CONTENT_TYPE);

        try
        {
            soyTemplateRenderer.render(resp.getWriter(), SERVER_SOY_MODULE_KEY, "testPages.pages.index", context);
        }
        catch (SoyException e)
        {
            if (e.getCause() instanceof IOException)
            {
                throw (IOException) e.getCause();
            }
            else
            {
                throw new ServletException(e);
            }
        }

        resp.getWriter().close();
    }
}