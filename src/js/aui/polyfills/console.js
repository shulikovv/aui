/**
 * AUI-2773
 *
 * The following shim for console is deprecated and to be removed in AUI 6.
 * We shouldn't be creating console.log if it doesn't exist; instead, we should avoid using it directly.
 * @start deprecated
 */

if (typeof window.console === 'undefined') {
    window.console = {
        messages: [],

        log: function (text) {
            this.messages.push(text);
        },

        show: function () {
            alert(this.messages.join('\n'));
            this.messages = [];
        }
    };
} else {
    // Firebug console - show not required to do anything.
    window.console.show = function () {};
}
