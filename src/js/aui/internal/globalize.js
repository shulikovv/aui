'use strict';

import createElement from '../create-element';
import objectAssign from 'object-assign';

function auiNamespace() {
    return createElement.apply(undefined, arguments);
};
var NAMESPACE = 'AJS';

export default function globalize (name, value) {
    if (window[NAMESPACE] !== auiNamespace) {
        window[NAMESPACE] = objectAssign(auiNamespace, window[NAMESPACE]);
    }

    return window[NAMESPACE][name] = value;
}
