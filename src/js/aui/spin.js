'use strict';

import $ from './jquery';

$.fn.spin = function() {
    const $this = $(this);
    const data = $this.data();
    const $spinnerDom = $(
        `<div class="aui-spinner spinner">
            <svg focusable="false" size="20" height="20" width="20" viewBox="0 0 20 20">
                <circle cx="10" cy="10" r="9"></circle>
            </svg>
        </div>`);

    $this.spinStop();
    $this.append($spinnerDom);

    const currentSpinnerTop = $spinnerDom.offset().top - $this.offset().top;
    const parentSpinnerHeight = $this.outerHeight();

    $spinnerDom.css({ top: parentSpinnerHeight / 2 - currentSpinnerTop });
    data.spinner = $spinnerDom;
};

$.fn.spinStop = function() {
    const $this = $(this);
    const data = $this.data();

    if (data.spinner) {
        data.spinner.remove();
    }
};
