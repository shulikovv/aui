import _ from 'underscore';

if (!window._) {
    window._ = _;
}

export default window._;
