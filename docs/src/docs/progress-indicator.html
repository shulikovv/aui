---
component: Progress indicator
layout: main-layout.html
analytics:
  pageCategory: component
  component: progress-indicator
---

<a href="https://design.atlassian.com/latest/product/components/progress-indicators/" class="aui-button aui-button-link docs-meta-link">Design guidelines</a>

<h3>Summary</h3>

<p>Progress indicators inform users that a system process is currently taking place that takes a predictable amount
    of time before it is finished.</p>

<h3>Status</h3>
<table class="aui summary">
    <tbody>
        <tr>
            <th>API status:</th>
            <td><span class="aui-lozenge aui-lozenge-success">general</span></td>
        </tr>
        <tr>
            <th>Included in AUI core?</th>
            <td><span class="aui-lozenge aui-lozenge-current">Not in core</span> You must explicitly require the web
                resource key.
            </td>
        </tr>
        <tr>
            <th>Web resource key:</th>
            <td class="resource-key" data-resource-key="com.atlassian.auiplugin:aui-progress-indicator">
                <code>com.atlassian.auiplugin:aui-progress-indicator</code></td>
        </tr>
        <tr>
            <th>AMD Module key:</th>
            <td class="resource-key">N/A</td>
        </tr>
        <tr>
            <th>Experimental since:</th>
            <td>5.2</td>
        </tr>
        <tr>
            <th>General API status:</th>
            <td>5.8</td>
        </tr>
        <tr>
            <th>Web component since:</th>
            <td>7.7</td>
        </tr>
    </tbody>
</table>
<h3>Examples</h3>

<div class="aui-flatpack-example">
    <aui-progressbar id="docs-progress-bar"></aui-progressbar>
    <div class="aui-group">
        <button id="toggle-progress-button" class="aui-button">Toggle</button>
        <button id="toggle-slow-progress-button" class="aui-button">Toggle Slow</button>
        <button id="toggle-shift-progress-button" class="aui-button">Add 10%</button>
    </div>
    <script type="text/javascript">
        AJS.$(function() {
            var cancelInterval;
            var PROGRESS_BAR_SHIFT = 10;
            var PROGRESS_BAR_MAX = 100;
            var TIME_INTERVAL = 200;

            var progressbar = document.getElementById("docs-progress-bar");
            progressbar.max = PROGRESS_BAR_MAX;

            AJS.$("#toggle-progress-button").click(function() {
                clearInterval(cancelInterval);
                progressbar.indeterminate = !progressbar.indeterminate;
            });

            AJS.$("#toggle-slow-progress-button").click(function() {
                clearInterval(cancelInterval);
                progressbar.indeterminate = !progressbar.indeterminate;
                progressbar.value = 0;
                cancelInterval = setInterval(function() {
                    progressbar.value += PROGRESS_BAR_SHIFT;
                    if (progressbar.value >= PROGRESS_BAR_MAX) {
                        clearInterval(cancelInterval);
                    }
                }, TIME_INTERVAL);
            });

            AJS.$("#toggle-shift-progress-button").click(function() {
                clearInterval(cancelInterval);
                progressbar.indeterminate = false;
                if (progressbar.value >= PROGRESS_BAR_MAX) {
                    progressbar.value = 0;
                } else {
                    progressbar.value += PROGRESS_BAR_SHIFT;
                }
            });
        });
    </script>
</div>

<h3>Usage</h3>

<p>
    To get an initial progress bar, simply add an <aui-docs-component>aui-progressbar</aui-docs-component> element
    to your page. You can adjust the current value and maximum value using either attributes or properties.
</p>

<aui-docs-example live-demo id="progress-example-attrs-and-props">
    <noscript is="aui-docs-code" type="text/html">
        <aui-progressbar id="with-attributes" value="5" max="100"></aui-progressbar>
        <aui-progressbar id="with-properties"></aui-progressbar>
    </noscript>
    <noscript is="aui-docs-code" type="text/js">
        let progress = document.getElementById("with-properties");
        progress.max = 42;
        progress.value = 7;
    </noscript>
</aui-docs-example>

<h3>Behaviour</h3>

<p>
    When the <code>indeterminate</code> state is set, the user will be notified that progress cannot be described
    in numeric terms. When unset, the progress bar will return to its previously set values.
</p>

<aui-docs-example live-demo id="progress-example-indeterminate">
    <noscript is="aui-docs-code" type="text/html">
        <aui-progressbar id="toggle-indeterminate" value="55" max="100" indeterminate></aui-progressbar>
        <button id="toggle-indeterminate-button" class="aui-button" aria-pressed="true">Toggle</button>
    </noscript>
    <noscript is="aui-docs-code" type="text/js">
        let $progress = jQuery("#toggle-indeterminate");
        let $button = jQuery("#toggle-indeterminate-button");
        $button.on("click", function() {
            if ($button.attr('aria-pressed')) {
                $button.removeAttr('aria-pressed');
                $progress.removeAttr('indeterminate');
            } else {
                $button.attr('aria-pressed', 'true');
                $progress.attr('indeterminate', '');
            }
        });
    </noscript>
</aui-docs-example>

<h3>API reference</h3>

<table class="aui">
    <caption><aui-docs-component>aui-progressbar</aui-docs-component></caption>
    <thead>
    <tr>
        <th>Name</th>
        <th>Attribute</th>
        <th>Property</th>
        <th>Type</th>
        <th>Default</th>
        <th class="description">Description</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td class="no-wrap"><code>max</code></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
        <td>Number</td>
        <td><code>1</code></td>
        <td>
            An integer or floating point number for the largest number allowed in <code>value</code>.
        </td>
    </tr>
    <tr>
        <td><code>value</code></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
        <td>Number</td>
        <td><code>0</code></td>
        <td>
            An integer or floating point number representing the current progress.
            <br/>
            The value will be conveyed to the user as a percentage of the <code>max</code>
            value, both visually and in assistive devices.
        </td>
    </tr>
    <tr>
        <td><code>indeterminate</code></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
        <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
        <td>Boolean</td>
        <td><code>false</code></td>
        <td>
            Set to <code>true</code> to convey that the current progress cannot be determined at this point in time.
        </td>
    </tr>
    </tbody>
</table>

<h4>AJS.progressBars <span class="aui-lozenge aui-lozenge-error">Deprecated</span></h4>
<p>
    An imperative API exists to update static progress bar HTML.
    In almost every case, the web component API is simpler, and should be the preferred method of using this component
    going forward.
</p>
<table class="aui" id="progress-indicators-table">
    <thead>
    <tr>
        <th>Function</th>
        <th>Arguments</th>
        <th class="description">Description</th>
        <th>Example Usage</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>update <span class="aui-lozenge aui-lozenge-error">Deprecated</span></td>
        <td>element, value</td>
        <td>updates the specified progress bar to the specified value.
            <ul>
                <li>The element argument can be either a jQuery object a selector string or a javascript node.</li>
                <li>The value must be between 0 and 1</li>
            </ul>
        </td>
        <td>
            <noscript is="aui-docs-code" type="text/js">AJS.progressBars.update("progress-bar-id", 0.4);</noscript>
        </td>
    </tr>
    <tr>
        <td>setIndeterminate <span class="aui-lozenge aui-lozenge-error">Deprecated</span></td>
        <td>element</td>
        <td> Sets a determinate progress bar back to its indeterminate state. NOTE: This will lose any progress on
            the progress bar. To retain the current progress you can read the 'data-value' attribute on the DOM
            Element.
        </td>
        <td>
            <noscript is="aui-docs-code" type="text/js">AJS.progressBars.setIndeterminate("progress-bar-id");</noscript>
        </td>
    </tr>
    </tbody>
</table>
