---
component: Dialog2
layout: main-layout.html
analytics:
  pageCategory: component
  component: dialog2
---

<a href="https://design.atlassian.com/latest/product/components/modal-dialog/" class="aui-button aui-button-link docs-meta-link">Design guidelines</a>

<h3>Summary</h3>
<p> Modal dialogs are used to get a response from a user or reveal critical information that cannot be ignored.</p>

<h3>Status</h3>
<table class="aui summary">
    <tbody>
        <tr>
            <th>API status:</th>
            <td><span class="aui-lozenge aui-lozenge-success">general</span></td>
        </tr>
        <tr>
            <th>Included in AUI core?</th>
            <td><span class="aui-lozenge aui-lozenge-current">Not in core</span> You must explicitly require the web resource key.</td>
        </tr>
        <tr>
            <th>Web resource key:</th>
            <td class="resource-key" data-resource-key="com.atlassian.auiplugin:dialog2"><code>com.atlassian.auiplugin:dialog2</code></td>
        </tr>
        <tr>
            <th>Experimental since:</th>
            <td>5.3</td>
        </tr>
        <tr>
            <th>General API status:</th>
            <td>5.8</td>
        </tr>
    </tbody>
</table>

<aui-docs-contents></aui-docs-contents>

<h3>Examples</h3>

<h4>Anatomy of a dialog</h4>
<p>In this example, you can see all the various pieces of the dialog's HTML pattern.</p>
<aui-docs-example id="static-dialog-example" live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <!--
            Renders a static dialog.
            To ensure the dialog is not rendered inline in the page, add:
             * class="aui-layer"
             * aria-hidden="true"
            to this element.
        -->
        <section id="static-dialog" class="aui-dialog2 aui-dialog2-medium" role="dialog">
            <!-- Dialog header -->
            <header class="aui-dialog2-header">
                <!-- The dialog's title -->
                <h2 class="aui-dialog2-header-main">The modal dialog title</h2>
                <!-- Actions to render on the right of the header -->
                <div class="aui-dialog2-header-secondary">
                    <form class="aui" action="#">
                        <input id="demo-search" class="text" type="search" name="search">
                    </form>
                </div>
                <!-- Close icon -->
                <a class="aui-dialog2-header-close">
                    <span class="aui-icon aui-icon-small aui-iconfont-close-dialog">Close</span>
                </a>
            </header>
            <!-- Main dialog content -->
            <div class="aui-dialog2-content">
                <p>Content for the modal dialog.</p>
            </div>
            <!-- Dialog footer -->
            <footer class="aui-dialog2-footer">
                <!-- Actions to render on the right of the footer -->
                <div class="aui-dialog2-footer-actions">
                    <button class="aui-button aui-button-primary">Okay</button>
                    <button class="aui-button">Next</button>
                    <button class="aui-button aui-button-link">Close</button>
                </div>
                <!-- Hint text is rendered on the left of the footer -->
                <div class="aui-dialog2-footer-hint">This is a hint.</div>
            </footer>
        </section>
    </noscript>
    <noscript type="text/js">
        AJS.$("#static-dialog").on("click submit", function(e) {
            e.preventDefault();
            e.stopImmediatePropagation();
        });
    </noscript>
    <noscript type="text/css">
        #static-dialog-example .aui-live-demo {
            position: relative;
            background-color: #999;
        }
        #static-dialog {
            margin-top: 100px; /* to push it inside the example box. */
        }
    </noscript>
</aui-docs-example>

<p>
    On its own, the dialog HTML pattern itself is static &mdash; the contents are added to the page inline.
    To ensure the dialog does not get rendered to the page, you should add <code>class="aui-layer"</code>
    and <code>aria-hidden="true"</code> to the dialog element.
</p>

<p>
    Note that in the footer, the hint text (<code>class="aui-dialog2-footer-hint"</code>) should be placed
    in the DOM below the footer actions (<code>class="aui-dialog2-footer-actions"</code>) even though the hint text
    appears to the left of the footer actions.
</p>


<h4>Opening and closing a dialog</h4>
<p>You can use JavaScript to make the dialog open and close.</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <!-- Create a trigger which will be used by the JavaScript -->
        <button id="dialog-show-button" class="aui-button">Show dialog</button>

        <section id="demo-dialog" class="aui-dialog2 aui-dialog2-small aui-layer" role="dialog" aria-hidden="true">
            <header class="aui-dialog2-header">
                <h2 class="aui-dialog2-header-main">Captain...</h2>
                <a class="aui-dialog2-header-close">
                    <span class="aui-icon aui-icon-small aui-iconfont-close-dialog">Close</span>
                </a>
            </header>
            <div class="aui-dialog2-content">
                <p>We've detected debris of some sort in a loose orbit.</p>
                <p>I suggest we beam a section aboard for analysis...</p>
            </div>
            <footer class="aui-dialog2-footer">
                <div class="aui-dialog2-footer-actions">
                    <button id="dialog-submit-button" class="aui-button aui-button-primary">Make it so</button>
                </div>
            </footer>
        </section>
    </noscript>
    <noscript is="aui-docs-code" type="text/js">
        // Shows the dialog when the "Show dialog" button is clicked
        AJS.$("#dialog-show-button").click(function(e) {
            e.preventDefault();
            AJS.dialog2("#demo-dialog").show();
        });

        // Hides the dialog
        AJS.$("#dialog-submit-button").click(function (e) {
            e.preventDefault();
            AJS.dialog2("#demo-dialog").hide();
        });
    </noscript>
</aui-docs-example>

<h4>A warning dialog</h4>

<p>
    Use this dialog type when you're representing a destructive action, and want the end-user to think more carefully
    about how they proceed.
</p>

<aui-docs-example live-demo>
    <noscript is="aui-docs-code" type="text/html">
        <button id="warning-dialog-show-button" class="aui-button">Show warning dialog</button>

        <section id="demo-warning-dialog" class="aui-dialog2 aui-dialog2-medium aui-dialog2-warning aui-layer" role="dialog" aria-hidden="true">
            <header class="aui-dialog2-header">
                <h2 class="aui-dialog2-header-main">Confirm you want to delete this thing</h2>
                <a class="aui-dialog2-header-close">
                    <span class="aui-icon aui-icon-small aui-iconfont-close-dialog">Close</span>
                </a>
            </header>
            <div class="aui-dialog2-content">
                <p>If you do this, there's no going back. Are you certain that you want this thing to be gone forever?</p>
            </div>
            <footer class="aui-dialog2-footer">
                <div class="aui-dialog2-footer-actions">
                    <button id="warning-dialog-confirm" class="aui-button aui-button-primary">Delete the thing</button>
                    <button id="warning-dialog-cancel" class="aui-button aui-button-link">Cancel</button>
                </div>
            </footer>
        </section>
    </noscript>
    <noscript is="aui-docs-code" type="text/js">
        // Shows the warning dialog when the "Show warning dialog" button is clicked
        AJS.$("#warning-dialog-show-button").click(function (e) {
            e.preventDefault();
            AJS.dialog2("#demo-warning-dialog").show();
        });

        AJS.$(document).on("click", "#demo-warning-dialog button", function (e) {
            e.preventDefault();
            AJS.dialog2("#demo-warning-dialog").hide();
        });
    </noscript>
</aui-docs-example>


<h3>Options</h3>

<h4>HTML attributes</h4>
<p>Dialog2 configuration options are expressed through markup.</p>
<table class="aui">
    <tbody>
    <tr>
        <th scope="col">Class</th>
        <th scope="col">Description</th>
        <th scope="col">Example Usage</th>
    </tr>
    <tr>
        <td><code>aui-dialog2-small | aui-dialog2-medium | aui-dialog2-large | aui-dialog2-xlarge</code></td>
        <td>Controls the size of the dialog according to ADG size specifications.</td>
        <td>
            <noscript is="aui-docs-code" type="text/html">
                <section class="aui-dialog2 aui-dialog2-small"
                         role="dialog"
                         aria-hidden="true">
                    <!-- inner content -->
                </section>
            </noscript>
        </td>
    </tr>
    <tr>
        <td><code>.aui-dialog2-warning</code></td>
        <td>Gives the dialog's header a red background color.</td>
        <td>
            <noscript is="aui-docs-code" type="text/html">
                <section class="aui-dialog2 aui-dialog2-warning"
                         role="dialog"
                         aria-hidden="true">
                    <!-- inner content -->
                </section>
            </noscript>
        </td>
    </tr>
    </tbody>
</table>

<table class="aui" id="dialog-attributes">
    <thead>
    <tr>
        <th>Attribute</th>
        <th>Values</th>
        <th class="description">Description</th>
        <th>Example Usage</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><code>data-aui-modal</code></td>
        <td><code>true</code></td>
        <td>Specifies that the dialog is modal. Modal dialogs have no close icon in the top right corner, and cannot be
            closed by clicking on the blanket behind it.
        </td>
        <td>
            <noscript is="aui-docs-code" type="text/html">
                <section class="aui-dialog2"
                         data-aui-modal="true"
                         role="dialog"
                         aria-hidden="true">
                    <!-- inner content -->
                </section>
            </noscript>
        </td>
    </tr>
    <tr>
        <td><code>data-aui-remove-on-hide</code></td>
        <td><code>true</code></td>
        <td>Specifies that the dialog element should be removed from the DOM when it is hidden, either by clicking on
            the close icon, clicking on the blanket behind the dialog, or calling the hide() method.
        </td>
        <td>
            <noscript is="aui-docs-code" type="text/html">
                <section class="aui-dialog2"
                         data-aui-remove-on-hide="true"
                         role="dialog"
                         aria-hidden="true">
                    <!-- inner content -->
                </section>
            </noscript>
        </td>
    </tr>
    <tr>
        <td><code>data-aui-focus-selector</code></td>
        <td><code>selector</code></td>
        <td>Controls the element that is focussed when the dialog is opened.</td>
        <td>
            <noscript is="aui-docs-code" type="text/html">
                <section class="aui-dialog2"
                         data-aui-focus-selector=".aui-dialog2-content :input:visible:enabled"
                         role="dialog"
                         aria-hidden="true">
                    <!-- inner content -->
                </section>
            </noscript>
        </td>
    </tr>
    </tbody>
</table>


<h4>API</h4>
<p>To get a reference to the API for a dialog2 instance, call AJS.dialog2(selector), where selector
    can be a selector string, DOM node, or jQuery element.</p>
<noscript is="aui-docs-code" type="text/js">var demoDialog = AJS.dialog2("#demo-dialog");</noscript>
<h4>Methods</h4>
<table class="aui" id="dialog-methods">
    <thead>
    <tr>
        <th>Method</th>
        <th class="description">Description</th>
        <th>Example Usage</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><code>show</code></td>
        <td>Shows a dialog.</td>
        <td>
            <noscript is="aui-docs-code" type="text/js">AJS.dialog2("#demo-dialog").show();</noscript>
        </td>
    </tr>
    <tr>
        <td><code>hide</code></td>
        <td>Hides a dialog.</td>
        <td>
            <noscript is="aui-docs-code" type="text/js">AJS.dialog2("#demo-dialog").hide();</noscript>
        </td>
    </tr>
    <tr>
        <td><code>remove</code></td>
        <td>Removes the dialog from the DOM.</td>
        <td>
            <noscript is="aui-docs-code" type="text/js">AJS.dialog2("#demo-dialog").remove();</noscript>
        </td>
    </tr>
    </tbody>
</table>


<h4>Events</h4>
<p>Events are triggered when dialogs are shown or closed. These can be listened to for a single
    dialog instance, or for all dialogs.</p>
<table class="aui" id="dialog-events">
    <thead>
    <tr>
        <th>Event</th>
        <th class="description">Description</th>
        <th>Example Usage</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><code>show</code></td>
        <td>Triggered when a dialog instance is shown.</td>
        <td><noscript is="aui-docs-code" type="text/js">
            AJS.dialog2("#demo-dialog").on("show", function() {
                console.log("demo-dialog was shown");
            });
        </noscript>
        </td>
    </tr>
    <tr>
        <td><code>hide</code></td>
        <td>Triggered when a dialog instance is hidden.</td>
        <td><noscript is="aui-docs-code" type="text/js">
            AJS.dialog2("#demo-dialog").on("hide", function() {
                console.log("demo-dialog was hidden");
            });
        </noscript>
        </td>
    </tr>
    <tr>
        <td><code>global show</code></td>
        <td>Triggered when any dialog is shown.</td>
        <td><noscript is="aui-docs-code" type="text/js">
            AJS.dialog2.on("show", function() {
                console.log("a dialog was shown");
            });
        </noscript>
        </td>
    </tr>
    <tr>
        <td><code>global hide</code></td>
        <td>Triggered when any dialog is hidden.</td>
        <td><noscript is="aui-docs-code" type="text/js">
            AJS.dialog2.on("hide", function() {
                console.log("a dialog was hidden");
            });
        </noscript>
        </td>
    </tr>
    </tbody>
</table>
