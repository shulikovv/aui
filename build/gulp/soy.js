'use strict';

var del = require('del');
var glob = require('glob');
var gulp = require('gulp');
var opts = require('gulp-auto-task').opts();
var path = require('path');
var soy = require('../lib/soy');
var wrap = require('../lib/wrap-file');

// Fix so that soy code exports properly to the window when wrapped in an iife.
var wrapAui = wrap('(function(){\naui = window.aui = window.aui || {}\n', '\n}());');
var wrapAtl = wrap('(function(){\n', '\n}());');
var wrapGoog = wrap('(function(){\ngoog = window.goog = window.goog || {};\n', '\nwindow.soy = soy;\nwindow.soydata = soydata;\n}());');

var outdir = path.join(opts.root, '.tmp', 'compiled-soy');

function cleanSoy (done) {
    del([outdir]).then(() => done());
}

function buildSoy (done) {
    soy({
        args: {
            basedir: path.join(opts.root, 'src/soy'),
            glob: '**.soy',
            i18n: path.join(opts.root, 'src/i18n/aui.properties'),
            outdir
        }
    });

    glob.sync(path.join(outdir, '*.js')).forEach(file => wrapAui(file));

    wrapAtl(require.resolve('@atlassian/soy-template-plugin-js/src/js/atlassian-deps.js'), path.join(outdir, 'atlassian-deps.js'));
    wrapGoog(require.resolve('@atlassian/soy-template-plugin-js/src/js/soyutils.js'), path.join(outdir, 'soyutils.js'));

    done();
}

module.exports = gulp.series(
    cleanSoy,
    buildSoy
);
