'use strict';

var del = require('del');

module.exports = function cleanProject (done) {
    del(['.tmp', 'dist', 'lib']).then(() => done());
};
