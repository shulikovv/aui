'use strict';

var gat = require('gulp-auto-task');
var gulp = require('gulp');
var pluginWrm = require('./plugin/wrm');

module.exports = gulp.series(
    gat.load('soy'),
    pluginWrm
);
