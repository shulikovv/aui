'use strict';

var gulp = require('gulp');

module.exports = function flatappSoyAssets () {
    return gulp.src('.tmp/flatapp/src/**/*')
        .pipe(gulp.dest('.tmp/flatapp/target'));
};
