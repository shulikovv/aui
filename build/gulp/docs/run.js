var docsOpts = require('../../lib/docs-opts');
var gat = require('gulp-auto-task');
var gulp = require('gulp');
var gulpWebserver = require('gulp-webserver');

var opts = gat.opts();
var { host, port } = Object.assign(docsOpts, opts);

module.exports = gulp.series(
    gat.load('docs/build'),
    function docsWatchServe () {
        return gulp.src('.tmp')
            .pipe(gulpWebserver({
                host,
                port,
                livereload: false,
                open: './docs/dist',
            }));
    }
);
