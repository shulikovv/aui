'use strict';

const docsOpts = require('../../lib/docs-opts');
const gulpOpts = require('gulp-auto-task').opts();
const del = require('del');
const metalsmith = require('metalsmith');
const metalsmithLayouts = require('metalsmith-layouts');
const metalsmithMarkdown = require('metalsmith-markdown');
const metalsmithTextReplace = require('metalsmith-text-replace');
const pkg = require('../../../package.json');

module.exports = function docsMetalsmith (done) {
    let opts = Object.assign({}, docsOpts, gulpOpts);
    let version = opts.docsVersion || pkg.version;

    var localDistUri = `//${opts.host}:${opts.port}/dist/aui`;
    var cdnDistUri = `//unpkg.com/${pkg.name}@${version}/dist/aui`;

    var ms = metalsmith('.tmp/docs')
        .destination('./dist')
        .use(metalsmithMarkdown())
        .use(metalsmithLayouts({
            directory: 'layouts',
            engine: 'handlebars'
        }))
        .use(metalsmithTextReplace({
            '**/**/*.html': [{
                find: /\$\{project\.version\}/g,
                replace: version
            }, {
                find: /\$\{dist\.location\}/g,
                replace: opts.localdist ? localDistUri : cdnDistUri
            }]
        }));

    ms.build(function (err) {
        if (err) {
            throw err;
        }
        del([
            '.tmp/docs/dist/js/*',
            '.tmp/docs/dist/scripts/*',
            '.tmp/docs/dist/css/*.less',
            '.tmp/docs/dist/styles/*.less'
        ]).then(() => done());
    });
};
