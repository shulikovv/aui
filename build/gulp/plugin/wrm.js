'use strict';

var fs = require('fs');
var galv = require('galvatron');
var gat = require('gulp-auto-task');
var gulpTap = require('gulp-tap');
var md5 = require('md5');
var mkdirp = require('mkdirp');
var path = require('path');
var libFilterPluginExcludeJs = require('../../lib/plugin-exclude-js');
var libTraceMap = require('../../lib/trace-map');

var auiAdgPath = path.resolve(path.join(__dirname, '..', '..', '..'));
var auiAdgJsPath = path.join(auiAdgPath, 'src', 'js');
var opts = gat.opts();

function pathToDashSeparated (file) {
    file = file.replace('.js', '');
    file = file.replace(/[^a-z0-9\-]/gi, '-');
    file = file.replace(/-+/g, '-');
    return file;
}

function npmPackageJsonPath (file) {
    var dirs = file.split(path.sep);
    var lastIndex = dirs.lastIndexOf('node_modules');
    var nodeModulesDirs = dirs.slice(0, lastIndex + 1);
    var isScopedPackage = dirs[lastIndex + 1] && dirs[lastIndex + 1][0] === '@';
    var packageDirs = nodeModulesDirs.concat(dirs.slice(lastIndex + 1, lastIndex + (isScopedPackage ? 3 : 2)));
    return path.join(process.cwd(), packageDirs.join(path.sep), 'package.json');
}

function isNpmPackage (file) {
    var dirs = file.split(path.sep);
    return dirs.indexOf('node_modules') !== -1 && fs.existsSync(npmPackageJsonPath(file));
}

function npmPackageToDashSeparated (file) {
    var packageJsonPath = npmPackageJsonPath(file);
    var pathWithinPackage = path.resolve(file).substring(path.dirname(packageJsonPath).length + path.sep.length);
    var packageJson = require(packageJsonPath);

    var packageKey = [];
    packageKey.push(packageJson.name.replace('/', '-'));
    if (packageJson.name !== '@atlassian/aui') {
        packageKey.push(packageJson.version.match(/^\d+\.\d+\.\d+$/) ? packageJson.version : md5(packageJson.version));
    }
    packageKey.push(pathToDashSeparated(pathWithinPackage));

    return packageKey.join('-');
}

function pathToKey (file) {
    var dashSeparated = isNpmPackage(file) ? npmPackageToDashSeparated(file) : pathToDashSeparated(file);
    return `internal-${dashSeparated}`;
}

function pathToName (file) {
    return `internal-${pathToDashSeparated(file)}.js`;
}

var declaredKeys = [];

function xml (filePath, deps) {
    const key = pathToKey(filePath);

    const wrDepsOut = deps.map(dep => `        <dependency>com.atlassian.auiplugin:${pathToKey(dep)}</dependency>`);
    return `
    <web-resource key="${key}" name="${key}">
        <transformation extension="js">
            <transformer key="jsI18n" />
        </transformation>
        ${wrDepsOut.join('\n')}
        <resource type="download" name="${pathToName(filePath)}" location="${filePath}" />
    </web-resource>`;
}

module.exports = function pluginWrm () {
    var filterPluginExcludeJs = libFilterPluginExcludeJs();

    var isStart = true;
    var modulesPath = path.join(auiAdgPath, 'integration', 'plugin', 'src', 'main', 'resources', 'META-INF', 'plugin-descriptor', 'es6.xml');
    var modulesFd;

    return galv.trace(path.join(auiAdgJsPath, 'aui*.js'), {
        map: libTraceMap(opts.root)
    }).createStream()
        .pipe(filterPluginExcludeJs)
        .on('data', function () {
            if (isStart) {
                isStart = false;
                if (!fs.existsSync(path.dirname(modulesPath))) {
                    mkdirp.sync(path.dirname(modulesPath));
                }
                modulesFd = fs.openSync(modulesPath, 'w');
                fs.writeSync(modulesFd, '<extra-modules>\n');
            }
        })
        .pipe(gulpTap(function (file) {
            var filePath = path.relative(auiAdgPath, file.path);
            var deps = file.imports.map(function (imp) {
                return path.relative(auiAdgPath, imp.path);
            });
            var key = pathToKey(filePath);

            if (declaredKeys.indexOf(key) !== -1) {
                return;
            }

            declaredKeys.push(key);

            fs.writeSync(modulesFd, xml(filePath, deps));
        }))
        .on('end', function () {
            fs.writeSync(modulesFd, '</extra-modules>\n');
            fs.closeSync(modulesFd);
        });
};
