#!/bin/bash
set -x
set -e

BRANCH=$1
RELEASE_VERSION=$2
NEXT_VERSION=$3
TYPE=$4

# Check the appropriate parameters were provided to the script.
if [ -z "${BRANCH}" -o -z "${RELEASE_VERSION}" -o -z "${NEXT_VERSION}" ]; then
  echo "Usage: $(basename ${0}) [branch] [release-version] [next-version]"
  exit 1
fi

# Set optional script parameters
# TODO: Pass + map the data from aui-versions in here so we can tag the Node releases appropriately.
if [ -z "${TYPE}" ]; then
    TYPE="latest"
fi

# Check that we have the prerequisite commands for releasing
command -v npm >/dev/null 2>&1 || { echo >&2 "Releasing AUI requires npm but it's not installed. Aborting."; exit 2; }
command -v git >/dev/null 2>&1 || { echo >&2 "Releasing AUI requires git but it's not installed. Aborting."; exit 2; }
command -v mvn >/dev/null 2>&1 || { echo >&2 "Releasing AUI requires Maven but it's not installed. Aborting."; exit 2; }

# Set and check the working directory
basedir=$(pwd)
auidir=${basedir}

if [ ! -d "${auidir}/node_modules" ]; then
    echo >&2 "Releasing AUI requires that Node dependencies are installed. Aborting."; exit 3
fi

cd ${auidir}
if [ -n "$(git status --porcelain)" ]; then
    echo >&2 "Releasing AUI requires a clean git working directory. Aborting."; exit 4
fi

##
# A utility function for bumping the versions of our deliverables in our monorepo
#
function bump {
    cd ${auidir}
    npm version $1 --no-git-tag-version
    cd ${auidir}/integration
    mvn versions:set --batch-mode -DnewVersion=$1 -DgenerateBackupPoms=false
}

##
# Bump AUI versions in preparation for building everything
#
bump ${RELEASE_VERSION}
cd ${auidir}
git commit -am "Release ${RELEASE_VERSION}."
git tag -a ${RELEASE_VERSION} -m "${RELEASE_VERSION}"

##
# Build lib
#
cd ${auidir}
$(npm bin)/gulp lib

##
# Build the flat distribution.
#
cd ${auidir}
$(npm bin)/gulp dist/compress --no-color
distzip="${auidir}/.tmp/aui-flat-pack-${RELEASE_VERSION}.zip"

##
# Build the P2 plugin
#
cd ${auidir}
$(npm bin)/gulp plugin --no-color

##
# Build and deploy the Node package
# Generation of files is thanks to the prepublishOnly step in package.json
#
cd ${auidir}
npm set @atlassian:registry=https://packages.atlassian.com/api/npm/atlassian-npm/
npm publish --@atlassian:registry=https://packages.atlassian.com/api/npm/atlassian-npm/ --tag ${TYPE}
npm set @atlassian:registry=https://registry.npmjs.org
npm publish --@atlassian:registry=https://registry.npmjs.org --tag ${TYPE}

##
# Deploy the flat distribution and P2 plugin
#
cd ${auidir}/integration
mvn clean deploy --batch-mode -DskipTests
mvn deploy:deploy-file --batch-mode -DskipTests \
                       -DgroupId=com.atlassian.aui \
                       -DartifactId=aui-flat-pack \
                       -Dversion="${RELEASE_VERSION}" \
                       -Dpackaging=zip \
                       -Dfile=${distzip} \
                       -DrepositoryId=maven-atlassian-com \
                       -Durl=https://packages.atlassian.com/maven/public \


##
# If we get to here, the publish should have succeeded, so we can commit to the new version.
# Literally.
#
bump ${NEXT_VERSION}
cd ${auidir}
git commit -am "Prepare for ${NEXT_VERSION}."
git push git@bitbucket.org:atlassian/aui.git ${RELEASE_VERSION}
git push git@bitbucket.org:atlassian/aui.git ${BRANCH}
