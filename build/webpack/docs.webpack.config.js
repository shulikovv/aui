const path = require('path');
const merge = require('webpack-merge');

const { librarySkeleton } = require('./webpack.skeleton');
const docsSrc = path.resolve('docs', 'src', 'entry', 'aui-docs.js');

module.exports = merge([
    librarySkeleton,

    {
        entry: {
            'index': docsSrc,
        },

        externals: [
            {
                'jquery': 'jQuery',
            }
        ],

        output: {
            path: path.resolve('.tmp', 'docs', 'dist')
        },
    }
]);
