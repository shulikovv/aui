'use strict';

import $ from '../../../src/js/aui/jquery';
import helpers from '../../helpers/all';
import skate from '../../../src/js/aui/internal/skate';
import '../../../src/js/aui/button';
import '../../../src/js/aui/spin';

describe('aui/spin', () => {
    let $testFixture;
    const SPINNER_SELECTOR = '.aui-spinner';

    beforeEach(() => {
        $testFixture = $('#test-fixture');
    });

    describe('basic functionality - ', () => {
        let $buttonShow;
        let $buttonHide;

        beforeEach(() => {
            $buttonShow = $('<button id="show-spinner"></button>');
            $buttonHide = $('<button id="hide-spinner"></button>');
            const $spinner = $('<span id="spinner"></span>');

            $testFixture.append($buttonShow);
            $testFixture.append($buttonHide);
            $testFixture.append($spinner);

            $buttonShow.on('click', $spinner.spin.bind($spinner));
            $buttonHide.on('click', $spinner.spinStop.bind($spinner));
        });

        it('should show spinner', () => {
            $buttonHide.click();
            $buttonShow.click();

            expect($(SPINNER_SELECTOR).length).to.equal(1);
        });

        it('should hide spinner', () => {
            $buttonShow.click();
            $buttonHide.click();

            expect($(SPINNER_SELECTOR).length).to.equal(0);
        });
    });

    describe('busy button - ', () => {
        let button;

        beforeEach(() => {
            button = helpers
                .fixtures({ button: '<button class="aui-button">Some Button</button>' })
                .button;
            skate.init(button);
        });

        afterEach(() => {
            $(button).remove();
        });

        it('should show spinner', () => {
            button.busy();
            expect($(SPINNER_SELECTOR).length).to.equal(1);
        });

        it('should hide spinner', () => {
            button.idle();
            expect($(SPINNER_SELECTOR).length).to.equal(0);
        });
    });

    describe('correct position - ', () => {
        let $container;

        beforeEach(() => {
            $container = $('<div style="width: 100px; height: 100px; display: inline-block"></div>');
            $testFixture.append($container);
        });

        it('should set correct spin position', () => {
            $container.spin();
            const spinnerPosition = $(`${ SPINNER_SELECTOR }>svg`).position();

            expect(spinnerPosition.left).to.equal(40);
            expect(spinnerPosition.top).to.equal(40);
        });
    });
});
