require(['jquery', './dropdown-server', './dropdown-fixtures'], function($, server, fixtures) {
    var { serverResponse, noSectionLabelResponse, opensSubmenuResponse, customAsyncTemplate } = fixtures;

    var removeServerResponse = function () {
        server.responses.forEach(function(response) {
            if (response.url.source === 'custom-async-dropdown') {
                var index = server.responses.indexOf(response);
                server.responses.splice(index, 1);
            }
        });
    };

    $(function() {
        var responseCodeInput = document.getElementById('response-code');
        var responseDataInput = document.getElementById('response-data');

        var sample1Button = document.getElementById('sample1');
        var sample2Button = document.getElementById('sample2');
        var sample3Button = document.getElementById('sample3');
        var resetButton = document.getElementById('async-reset');
        var restoreXhrButton = document.getElementById('restore-xhr');
        var form = document.getElementById('dd-custom-form');

        var toggleContainer = document.getElementById('dropdown-container');

        var applyChanges = function () {
            toggleContainer.innerHTML = customAsyncTemplate;
            server.respondWith(/custom-async-dropdown/, [
                parseInt(responseCodeInput.value),
                { 'Content-Type': 'application/json' },
                responseDataInput.value
            ]);
        };

        var handleChange = function () {
            removeServerResponse();
            applyChanges();
        };

        restoreXhrButton.addEventListener('click', function () {
            server.restore();
        });

        resetButton.addEventListener('click', handleChange);

        responseCodeInput.addEventListener('change', handleChange);

        responseDataInput.value = JSON.stringify(serverResponse);
        responseDataInput.addEventListener('change', handleChange);

        form.addEventListener('submit', function (e) {
            e.preventDefault();
            applyChanges()
        });

        sample1Button.addEventListener('click', function () {
            responseDataInput.value = JSON.stringify(serverResponse);
            handleChange();
        });

        sample2Button.addEventListener('click', function () {
            responseDataInput.value = JSON.stringify(noSectionLabelResponse);
            handleChange();
        });

        sample3Button.addEventListener('click', function () {
            responseDataInput.value = JSON.stringify(opensSubmenuResponse);
            handleChange();
        });

        applyChanges();
    });
});
